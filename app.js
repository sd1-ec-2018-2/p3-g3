var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var irc = require('irc');
var path = require('path');
var uniqid = require('uniqid');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static('public'));

var users = {};
var proxies = {};

var amqp = require('amqplib/callback_api');
var ampqlURL = 'amqp://kiwwtevk:isO9lQLTJQpg0M5N-M4H4UfU-tpTlI5v@woodpecker.rmq.cloudamqp.com/kiwwtevk';
var amqpConn, amqpCh;
var exchange = 'main';

/**
 * Estabelece uma conexão com o servidor AMQP
 */
amqp.connect(ampqlURL, function(err, conn) {

	conn.createChannel(function(err, ch) {
		
		amqpConn = conn;
		amqpCh = ch;
		amqpCh.assertExchange(exchange, 'topic', { durable: false });

		// Iniciar servidor apenas após receber conexão do AMQP
		http.listen(3000, function () {
			console.log('Example app listening on port 3000!');
		});

		// Iniciar conexão por socket
		io.on("connection", function(socket) {

			// Esperar join pelo socket
			socket.on("join", function(user) {

				// Adicionar usuário e socket na lista dos proxies
				proxies[user.id] = { user: user, socket: socket };

				// Enviar registro de conexão para o servidor
				sendToServer("log.info.register", { id: user.id });

				socket.on("send", function(message) {
					sendToServer("log.user."+user.id, { nick: user.nick, id: user.id, message: message });
				});

				// Esperar mensagens na fila do usuário
				listenFromServer("log.user."+user.id, function(response) {
					handleChatMessage(response);
				});

			});

		});

		// Registrar cliente IRC assim que receber conexão
		listenFromServer("log.info.register", function(response) {
			
			var proxy = proxies[response.id];
			var user = proxy.user;
			var client = new irc.Client(user.server, user.nick, { channels: [user.channel] });

			// Adicionar cliente no objeto proxy
			proxy.client = client;

			// ---------------------------------------------------------------------------------------------------- //
			// Eventos IRC

			client.addListener("message" + user.channel, function(from, message) {
				proxy.socket.emit("chat", message, from);
			});
		
			client.addListener("nick", function(oldnick, newnick, channels, message) {
				proxy.socket.emit("update", oldnick + " mudou seu nickname para " + newnick);
			});
		
			client.addListener("quit", function(nick, reason, channels, message) {
				proxy.socket.emit("update", nick + " se desconectou");
			});

			// ---------------------------------------------------------------------------------------------------- //

			// Liberar aplicação
			proxy.socket.emit("unlock");
			
		});

	});

});

/**
 * Envia uma mensagem para o AMQP
 * @param {*} command 
 * @param {*} message 
 */
function sendToServer(key, message) {
	amqpCh.publish(exchange, key, Buffer.from(JSON.stringify(message)));
}

/**
 * Espera a resposta de alguma fila no AMQP
 * @param {*} channel 
 * @param {*} callback 
 */
function listenFromServer(key, callback) {

	amqpCh.assertQueue('', { exclusive: true }, function(err, q) {
		
		amqpCh.bindQueue(q.queue, exchange, key);
		amqpCh.consume(q.queue, function(response) {
			callback(JSON.parse(response.content.toString()));
		}, { noAck: true });

	});
}

/**
 * Executar uma funcionalidade do cliente IRC
 * @param {*} proxy 
 * @param {*} commands 
 */
function executeIrcCommand(proxy, commands) {

	var command = commands[0];
	commands.splice(0, 1);

	var message = commands.join(" ").trim();

	if (command == "/nick") {
		console.log("irc.Client.send: nick", message);
		proxy.client.send("nick", message);
	
	} else if (command == "/quit") {
		console.log("irc.Client.send: quit", message);
		proxy.client.send("quit", message);

	} else if (false) {
	}
}

/**
 * Lidar com mensagem recebida pelo AMQP
 * @param {*} params 
 */
function handleChatMessage(params) {

	var proxy = proxies[params.id];
	var commands = params.message.split(" ");
	var command = commands[0];

	// Comando IRC
	if (command.charAt(0) == "/") {
		executeIrcCommand(proxy, commands);

	// Mensagem comum
	} else {
		console.log("irc.Client.say", params.message);
		proxy.client.say(proxy.client.opt.channels[0], params.message);
	}
}

/**
 * Rota inicial, direcionar para login ou inicializar chat em algum servidor
 */
app.get('/', function (req, res) {

	if (req.cookies.id) {
		res.sendFile(path.join(__dirname, '/index.html'));
	
	} else {
		res.sendFile(path.join(__dirname, '/login.html'));
	}

});

/**
 * Rota para login
 */
app.post("/login", function(req, res) {
	
	res.cookie("nick", req.body.nick);
	res.cookie("channel", req.body.channel);
	res.cookie("server", req.body.server);
	res.cookie("id", uniqid());
	res.redirect("/");

});